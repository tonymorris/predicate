0.0.4

* Fix type signature in `true` and `false`

0.0.3

* Add `true` and `false`
* Generalise some type signatures

0.0.2

* Relax lens dependency

0.0.1

* This change log starts
